import boto3

_cfn_client = boto3.client("cloudformation")

class CfnStackOutputInspector():
    
    def _retrieve_info(self, stack_name):
        response = _cfn_client.describe_stacks(StackName=stack_name)
        stack = response["Stacks"][0]
        return {
            output["OutputKey"]: output["OutputValue"]
            for output in stack["Outputs"]
        }
    
    def create(self, event, context):
        params = event["ResourceProperties"]["Properties"]
        if "StackName" in params:
            stack_name = params["StackName"]
            physical_resource_id  = f"CfnStackOutputInspector-{stack_name}"
            try:
                response_data = self._retrieve_info(stack_name)
                return physical_resource_id, response_data
            except Exception:
                raise Exception(f"Could Not Retrieve Information for Stack {stack_name}")
        raise Exception("Expected Property: StackName")

    def update(self, event, context):
        _, response_data = self.create(event, context)
        return None, response_data

    def delete(self, event, context):
        return
