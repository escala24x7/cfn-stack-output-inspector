from __future__ import print_function
from crhelper import CfnResource
import logging

from resources.cloudformation import CfnStackOutputInspector

logger = logging.getLogger(__name__)
# Initialise the helper, all inputs are optional, this example shows the defaults
helper = CfnResource(json_logging=False, log_level='DEBUG', boto_level='CRITICAL', sleep_on_delete=120, ssl_verify=None)

try:
    ## Init code goes here
    supported_resources = {
        "Custom::CfnStackOutputInspector": CfnStackOutputInspector()
    }
except Exception as e:
    helper.init_failure(e)


@helper.create
def create(event, context):
    logger.info("Got Create")
    # Optionally return an ID that will be used for the resource PhysicalResourceId, 
    # if None is returned an ID will be generated. If a poll_create function is defined 
    # return value is placed into the poll event as event['CrHelperData']['PhysicalResourceId']
    #
    # To add response data update the helper.Data dict
    # If poll is enabled data is placed into poll event as event['CrHelperData']
    # helper.Data.update({"test": "testdata"})
    resource_type = event["ResourceType"]
    resource = supported_resources.get(resource_type)
    if resource:
        physical_resource_id, response_data = resource.create(event, context)
        if response_data:
            helper.Data.update(response_data)
        return physical_resource_id
    raise Exception("Unsupported Custom Resource Type: {}".format(resource_type))


@helper.update
def update(event, context):
    logger.info("Got Update")
    # If the update resulted in a new resource being created, return an id for the new resource. CloudFormation will send
    # a delete event with the old id when stack update completes
    resource_type = event["ResourceType"]
    current_physical_resource_id = event["PhysicalResourceId"]
    resource = supported_resources.get(resource_type)
    if resource:
        physical_resource_id, response_data = resource.update(event, context)
        if response_data:
            helper.Data.update(response_data)
        if physical_resource_id and physical_resource_id != current_physical_resource_id:
            return physical_resource_id
        return
    raise Exception("Unsupported Custom Resource Type: {}".format(resource_type))


@helper.delete
def delete(event, context):
    logger.info("Got Delete")
    # Delete never returns anything. Should not fail if the underlying resources are already deleted. Desired state.
    resource_type = event["ResourceType"]
    resource = supported_resources.get(resource_type)
    if resource:
        resource.delete(event, context)
        return
    raise Exception("Unsupported Custom Resource Type: {}".format(resource_type))


@helper.poll_create
def poll_create(event, context):
    logger.info("Got create poll")
    # Return a resource id or True to indicate that creation is complete. if True is returned an id 
    # will be generated
    return True


def lambda_handler(event, context):
    helper(event, context)
